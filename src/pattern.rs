use std::ops::Deref;
use std::rc::Rc;
use std::sync::{Arc, Mutex};

use crate::span::{Boxed, Span};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Match {
    span: Span,
    groups: Vec<Span>,
}

impl Match {
    pub fn new(span: Span, groups: Vec<Span>) -> Self {
        Match { span, groups }
    }

    pub fn into_parts(self) -> (Span, Vec<Span>) {
        (self.span, self.groups)
    }

    pub fn push_group(&mut self, group: Span) {
        self.groups.push(group)
    }

    pub fn group(&self, group: usize) -> &Span {
        &self.groups[group]
    }

    pub fn groups(&self) -> &[Span] {
        &self.groups
    }

    pub fn start(&self) -> usize {
        self.span.start()
    }

    pub fn end(&self) -> usize {
        self.span.end()
    }

    pub fn src(&self) -> &Boxed<String> {
        self.span.src()
    }

    pub fn span(&self) -> &Span {
        &self.span
    }
}

impl Deref for Match {
    type Target = str;

    fn deref(&self) -> &str {
        self.span.as_ref()
    }
}

impl AsRef<str> for Match {
    fn as_ref(&self) -> &str {
        self.span.as_ref()
    }
}

impl<'a> PartialEq<&'a str> for Match {
    fn eq(&self, other: &&'a str) -> bool {
        self.as_ref() == *other
    }
}

impl<'a> PartialEq<Match> for &'a str {
    fn eq(&self, other: &Match) -> bool {
        *self == other.as_ref()
    }
}

pub trait Pattern: Sized {
    fn is_match(&self, span: &Span) -> bool {
        self.try_match(span).is_some()
    }

    fn try_match(&self, span: &Span) -> Option<Match>;

    fn and<P: Pattern>(self, other: P) -> And<Self, P> {
        And::new(self, other)
    }

    fn or<P: Pattern>(self, other: P) -> Or<Self, P> {
        Or::new(self, other)
    }

    fn maybe(self) -> Maybe<Self> {
        Maybe::new(self)
    }

    fn quiet(self) -> Quiet<Self> {
        Quiet::new(self)
    }

    fn zero_plus(self) -> ZeroPlus<Self> {
        ZeroPlus::new(self)
    }

    fn one_plus(self) -> OnePlus<Self> {
        OnePlus::new(self)
    }

    fn between(self, min: Option<usize>, max: Option<usize>) -> Between<Self> {
        Between::new(self, min, max)
    }

    fn capture(self) -> Capture<Self> {
        Capture::new(self)
    }
}

impl<P: Pattern> Pattern for Box<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        P::try_match(self, span)
    }
}

impl<P: Pattern> Pattern for Rc<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        P::try_match(self, span)
    }
}

impl<P: Pattern> Pattern for Arc<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        P::try_match(self, span)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Count(usize);

impl Count {
    pub fn new(count: usize) -> Self {
        Count(count)
    }
}

impl Default for Count {
    fn default() -> Self {
        Count::new(1)
    }
}

impl Pattern for Count {
    fn try_match(&self, span: &Span) -> Option<Match> {
        Some(Match::new(span.respan(span.start()..span.start() + self.0)?, vec![]))
    }
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash)]
pub struct True;

impl Pattern for True {
    fn try_match(&self, span: &Span) -> Option<Match> {
        Some(Match::new(span.clone(), vec![]))
    }
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, Hash)]
pub struct False;

impl Pattern for False {
    fn try_match(&self, _span: &Span) -> Option<Match> {
        None
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Predicate<F> {
    pred: F,
}

impl<F> Predicate<F>
where
    for<'a> F: Fn(&'a str) -> Option<usize>,
{
    pub fn new(pred: F) -> Self {
        Predicate { pred }
    }
}

impl<F> Pattern for Predicate<F>
where
    for<'a> F: Fn(&'a str) -> Option<usize>,
{
    fn try_match(&self, span: &Span) -> Option<Match> {
        (self.pred)(&span).map(|len| Match::new(span.respan(span.start()..span.start() + len).unwrap(), vec![]))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Char(char);

impl Char {
    pub fn new(ch: char) -> Self {
        Char(ch)
    }
}

impl Pattern for Char {
    fn try_match(&self, span: &Span) -> Option<Match> {
        span.chars().next().and_then(|ch| {
            if self.0 == ch {
                Some(Match::new(span.respan(span.start()..span.start() + 1).unwrap(), vec![]))
            } else {
                None
            }
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Str<'a>(&'a str);

impl<'a> Str<'a> {
    pub fn new(string: &'a str) -> Self {
        Str(string)
    }
}

impl<'a> Pattern for Str<'a> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        if self.0.len() <= span.len() && self.0 == &span[..self.0.len()] {
            Some(Match::new(
                span.respan(span.start()..span.start() + self.0.len()).unwrap(),
                vec![],
            ))
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Range(char, char);

impl Range {
    pub fn new(a: char, b: char) -> Self {
        Range(a, b)
    }
}

impl Pattern for Range {
    fn try_match(&self, span: &Span) -> Option<Match> {
        span.chars().next().and_then(|ch| {
            if ch >= self.0 && ch <= self.1 {
                Some(Match::new(span.respan(span.start()..span.start() + 1).unwrap(), vec![]))
            } else {
                None
            }
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct NotRange(char, char);

impl NotRange {
    pub fn new(a: char, b: char) -> Self {
        NotRange(a, b)
    }
}

impl Pattern for NotRange {
    fn try_match(&self, span: &Span) -> Option<Match> {
        span.chars().next().and_then(|ch| {
            if ch >= self.0 && ch <= self.1 {
                None
            } else {
                Some(Match::new(span.respan(span.start()..span.start() + 1).unwrap(), vec![]))
            }
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Any<'a>(&'a str);

impl<'a> Any<'a> {
    pub fn new(string: &'a str) -> Self {
        Any(string)
    }
}

impl<'a> Pattern for Any<'a> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        span.chars().next().and_then(|c0| {
            for c1 in self.0.chars() {
                if c0 == c1 {
                    return Some(Match::new(span.respan(span.start()..span.start() + 1).unwrap(), vec![]));
                }
            }
            None
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct NotAny<'a>(&'a str);

impl<'a> NotAny<'a> {
    pub fn new(string: &'a str) -> Self {
        NotAny(string)
    }
}

impl<'a> Pattern for NotAny<'a> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        span.chars().next().and_then(|c0| {
            for c1 in self.0.chars() {
                if c0 == c1 {
                    return None;
                }
            }
            Some(Match::new(span.respan(span.start()..span.start() + 1).unwrap(), vec![]))
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct And<P, Q>(P, Q);

impl<P: Pattern, Q: Pattern> And<P, Q> {
    pub fn new(a: P, b: Q) -> Self {
        And(a, b)
    }
}

impl<P: Pattern, Q: Pattern> Pattern for And<P, Q> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        self.0.try_match(span).and_then(|a| {
            let (a, mut groups) = a.into_parts();
            match self.1.try_match(&span.respan(a.end()..span.end()).unwrap()) {
                Some(b) => {
                    let (b, b_groups) = b.into_parts();
                    groups.extend_from_slice(&b_groups);
                    Some(Match::new(span.respan(a.start()..b.end()).unwrap(), groups))
                }
                None => None,
            }
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Or<P, Q>(P, Q);

impl<P: Pattern, Q: Pattern> Or<P, Q> {
    pub fn new(a: P, b: Q) -> Self {
        Or(a, b)
    }
}

impl<P: Pattern, Q: Pattern> Pattern for Or<P, Q> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        self.0.try_match(span).or_else(|| self.1.try_match(span))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Maybe<P>(P);

impl<P: Pattern> Maybe<P> {
    pub fn new(pat: P) -> Self {
        Maybe(pat)
    }
}

impl<P: Pattern> Pattern for Maybe<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        self.0
            .try_match(span)
            .or_else(|| Some(Match::new(span.respan(span.start()..span.start()).unwrap(), vec![])))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ZeroPlus<P>(P);

impl<P: Pattern> ZeroPlus<P> {
    pub fn new(pat: P) -> Self {
        ZeroPlus(pat)
    }
}

impl<P: Pattern> Pattern for ZeroPlus<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        let mut span = span.clone();
        let mut output = span.respan(span.start()..span.start()).unwrap();
        let mut groups = vec![];
        loop {
            if span.is_empty() {
                return Some(Match::new(output, groups));
            }
            match self.0.try_match(&span) {
                Some(out) => {
                    let (out, out_groups) = out.into_parts();
                    groups.extend_from_slice(&out_groups);
                    output = span.respan(output.start()..out.end()).unwrap();
                    span = span.respan(out.end()..span.end()).unwrap();
                }
                None => return Some(Match::new(output, groups)),
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct OnePlus<P>(P);

impl<P: Pattern> OnePlus<P> {
    pub fn new(pat: P) -> Self {
        OnePlus(pat)
    }
}

impl<P: Pattern> Pattern for OnePlus<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        let mut span = span.clone();
        let mut output = span.respan(span.start()..span.start()).unwrap();
        let mut groups = vec![];
        let mut one = false;
        loop {
            if span.is_empty() {
                if one {
                    return Some(Match::new(output, groups));
                } else {
                    return None;
                }
            }
            match self.0.try_match(&span) {
                Some(out) => {
                    let (out, out_groups) = out.into_parts();
                    groups.extend_from_slice(&out_groups);
                    output = span.respan(output.start()..out.end()).unwrap();
                    span = span.respan(out.end()..span.end()).unwrap();
                    one = true;
                }
                None if one => return Some(Match::new(output, groups)),
                _ => return None,
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Between<P> {
    pattern: P,
    min: usize,
    max: Option<usize>,
}

impl<P: Pattern> Between<P> {
    pub fn new(pattern: P, min: Option<usize>, max: Option<usize>) -> Self {
        Between {
            pattern,
            min: min.unwrap_or(0),
            max,
        }
    }
}

impl<P: Pattern> Pattern for Between<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        let mut span = span.clone();
        let mut output = span.respan(span.start()..span.start()).unwrap();
        let mut groups = vec![];
        let mut count = 0;
        loop {
            if span.is_empty() {
                if count >= self.min {
                    return Some(Match::new(output, groups));
                } else {
                    return None;
                }
            }
            if let Some(max) = self.max {
                if count >= self.min && count == max {
                    return Some(Match::new(output, groups));
                }
            }
            match self.pattern.try_match(&span) {
                Some(out) => {
                    let (out, out_groups) = out.into_parts();
                    groups.extend_from_slice(&out_groups);
                    output = span.respan(output.start()..out.end()).unwrap();
                    span = span.respan(out.end()..span.end()).unwrap();
                    count += 1;
                }
                None if count >= self.min => return Some(Match::new(output, groups)),
                _ => return None,
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Quiet<P>(P);

impl<P: Pattern> Quiet<P> {
    pub fn new(pat: P) -> Self {
        Quiet(pat)
    }
}

impl<P: Pattern> Pattern for Quiet<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        self.0.try_match(span).map(|matches| {
            let (_, groups) = matches.into_parts();
            Match::new(span.respan(span.start()..span.start()).unwrap(), groups)
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Capture<P>(P);

impl<P: Pattern> Capture<P> {
    pub fn new(pat: P) -> Self {
        Capture(pat)
    }
}

impl<P: Pattern> Pattern for Capture<P> {
    fn try_match(&self, span: &Span) -> Option<Match> {
        self.0.try_match(span).map(|matches| {
            let (out, out_groups) = matches.into_parts();
            let mut groups = vec![out.clone()];
            groups.extend_from_slice(&out_groups);
            Match::new(out, groups)
        })
    }
}

pub fn indent(indent: Option<usize>) -> (IndentEq, IndentDown, IndentUp) {
    let indent = Arc::new(Mutex::new(vec![indent.unwrap_or(0)]));
    (
        IndentEq {
            indent: Arc::clone(&indent),
        },
        IndentDown {
            indent: Arc::clone(&indent),
        },
        IndentUp { indent },
    )
}

#[derive(Debug, Clone)]
pub struct IndentEq {
    pub(self) indent: Arc<Mutex<Vec<usize>>>,
}

impl IndentEq {
    pub fn indent(&self) -> usize {
        *self
            .indent
            .lock()
            .expect("indent pattern mutex was poisoned")
            .last()
            .expect("indentation level underflowed")
    }
}

impl Pattern for IndentEq {
    fn try_match(&self, span: &Span) -> Option<Match> {
        if span.starts_with('\n') {
            let mut indent = 0;
            let mut offset = 1;
            for c in span.chars().skip(1) {
                if c == '\n' {
                    break;
                }
                if c.is_whitespace() {
                    indent += 1;
                    offset += c.len_utf8();
                } else {
                    break;
                }
            }
            if indent == self.indent() {
                Some(Match::new(
                    span.respan(span.start()..span.start() + offset).unwrap(),
                    vec![],
                ))
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[derive(Debug, Clone)]
pub struct IndentDown {
    pub(self) indent: Arc<Mutex<Vec<usize>>>,
}

impl IndentDown {
    pub fn indent(&self) -> usize {
        *self
            .indent
            .lock()
            .expect("indent pattern mutex was poisoned")
            .last()
            .expect("indentation level underflowed")
    }

    fn push(&self, indent: usize) {
        self.indent
            .lock()
            .expect("indent pattern mutex was poisoned")
            .push(indent)
    }

    fn pop(&self) -> usize {
        self.indent
            .lock()
            .expect("indent pattern mutex was poisoned")
            .pop()
            .expect("indentation level underflowed")
    }
}

impl Pattern for IndentDown {
    fn try_match(&self, span: &Span) -> Option<Match> {
        if span.starts_with('\n') {
            let mut indent = 0;
            let mut offset = 1;
            for c in span.chars().skip(1) {
                if c == '\n' {
                    break;
                }
                if c.is_whitespace() {
                    indent += 1;
                    offset += c.len_utf8();
                } else {
                    break;
                }
            }
            if indent < self.indent() {
                let popped = self.pop();
                if indent == self.indent() {
                    Some(Match::new(
                        span.respan(span.start()..span.start() + offset).unwrap(),
                        vec![],
                    ))
                } else {
                    self.push(popped);
                    None
                }
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[derive(Debug, Clone)]
pub struct IndentUp {
    pub(self) indent: Arc<Mutex<Vec<usize>>>,
}

impl IndentUp {
    pub fn indent(&self) -> usize {
        *self
            .indent
            .lock()
            .expect("indent pattern mutex was poisoned")
            .last()
            .expect("indentation level underflowed")
    }

    fn push(&self, indent: usize) {
        self.indent
            .lock()
            .expect("indent pattern mutex was poisoned")
            .push(indent)
    }
}

impl Pattern for IndentUp {
    fn try_match(&self, span: &Span) -> Option<Match> {
        if span.starts_with('\n') {
            let mut indent = 0;
            let mut offset = 1;
            for c in span.chars().skip(1) {
                if c == '\n' {
                    break;
                }
                if c.is_whitespace() {
                    indent += 1;
                    offset += c.len_utf8();
                } else {
                    break;
                }
            }
            if indent > self.indent() {
                self.push(indent);
                Some(Match::new(
                    span.respan(span.start()..span.start() + offset).unwrap(),
                    vec![],
                ))
            } else {
                None
            }
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sanity() {
        assert_eq!(Char::new('a').try_match(&Span::with_str(None, "arst")).unwrap(), "a");
        assert_eq!(Char::new('b').try_match(&Span::with_str(None, "arst")), None);
        assert_eq!(Str::new("ars").try_match(&Span::with_str(None, "arst")).unwrap(), "ars");
        assert_eq!(Str::new("rst").try_match(&Span::with_str(None, "arst")), None);
        assert_eq!(
            Any::new("foobar").try_match(&Span::with_str(None, "arst")).unwrap(),
            "a"
        );
        assert_eq!(NotAny::new("foobar").try_match(&Span::with_str(None, "arst")), None);
        assert_eq!(
            NotAny::new("foo").try_match(&Span::with_str(None, "arst")).unwrap(),
            "a"
        );
        assert_eq!(
            Range::new('a', 'c').try_match(&Span::with_str(None, "arst")).unwrap(),
            "a"
        );
        assert_eq!(Range::new('c', 'f').try_match(&Span::with_str(None, "arst")), None);
        assert_eq!(Count::new(3).try_match(&Span::with_str(None, "arst")).unwrap(), "ars");
        assert_eq!(True.try_match(&Span::with_str(None, "arst")).unwrap(), "arst");
        assert_eq!(False.try_match(&Span::with_str(None, "arst")), None);
    }

    #[test]
    fn combinators() {
        assert_eq!(
            Char::new('a')
                .or(Char::new('b'))
                .try_match(&Span::with_str(None, "bar"))
                .unwrap(),
            "b"
        );

        assert_eq!(
            Char::new('b')
                .and(Char::new('a'))
                .try_match(&Span::with_str(None, "bar"))
                .unwrap(),
            "ba"
        );

        assert_eq!(
            Char::new('a')
                .or(Char::new('b'))
                .try_match(&Span::with_str(None, "foo")),
            None,
        );

        assert_eq!(
            Char::new('b')
                .and(Char::new('a'))
                .try_match(&Span::with_str(None, "foo")),
            None,
        );

        assert_eq!(
            Str::new("foo")
                .quiet()
                .try_match(&Span::with_str(None, "foobar"))
                .unwrap(),
            "",
        );

        assert_eq!(Str::new("bar").quiet().try_match(&Span::with_str(None, "foobar")), None);

        assert_eq!(
            Str::new(".")
                .one_plus()
                .try_match(&Span::with_str(None, "..."))
                .unwrap(),
            "..."
        );

        assert_eq!(Str::new(".").one_plus().try_match(&Span::with_str(None, ",,,")), None);

        assert_eq!(
            Str::new(".")
                .zero_plus()
                .try_match(&Span::with_str(None, "..."))
                .unwrap(),
            "..."
        );

        assert_eq!(
            Str::new(".")
                .zero_plus()
                .try_match(&Span::with_str(None, ",,,"))
                .unwrap(),
            ""
        );

        assert_eq!(
            Str::new(".").maybe().try_match(&Span::with_str(None, "...")).unwrap(),
            "."
        );

        assert_eq!(
            Str::new(".").maybe().try_match(&Span::with_str(None, ",,,")).unwrap(),
            ""
        );

        assert_eq!(
            Str::new(".")
                .between(Some(1), Some(2))
                .try_match(&Span::with_str(None, "..."))
                .unwrap(),
            ".."
        );

        assert_eq!(
            Str::new(".")
                .between(Some(1), Some(2))
                .try_match(&Span::with_str(None, ".,,"))
                .unwrap(),
            "."
        );

        assert_eq!(
            Str::new(".")
                .between(Some(1), Some(2))
                .try_match(&Span::with_str(None, ",,,")),
            None,
        );
    }

    #[test]
    fn struct_pattern() {
        // #[derive(Pattern)]
        // #[pattern = "((foo)(bar))?"]
        // struct Pat;

        struct Pat(Maybe<And<Str<'static>, Str<'static>>>);

        impl Default for Pat {
            fn default() -> Self {
                Pat(Maybe::new(And::new(Str::new("foo"), Str::new("bar"))))
            }
        }

        impl Pattern for Pat {
            fn try_match(&self, span: &Span) -> Option<Match> {
                self.0.try_match(span)
            }
        }

        let span = Span::with_str(None, "foobar");
        assert_eq!(Pat::default().try_match(&span).unwrap(), "foobar");
    }
}
