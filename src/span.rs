use std::ops::{Deref, Range};
#[cfg(not(feature = "threads"))]
use std::rc::Rc;
#[cfg(feature = "threads")]
use std::sync::Arc;

#[cfg(not(feature = "threads"))]
pub type Boxed<T> = Rc<T>;

#[cfg(feature = "threads")]
pub type Boxed<T> = Arc<T>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Position {
    pub line: usize,
    pub col: usize,
    pub indent: usize,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Span {
    file: Option<Boxed<String>>,
    src: Boxed<String>,
    start: usize,
    end: usize,
}

impl Span {
    pub fn new(file: Option<Boxed<String>>, src: Boxed<String>, start: usize, end: usize) -> Option<Span> {
        if start >= src.len() || end < start || end > src.len() {
            return None;
        }

        Some(Span { file, src, start, end })
    }

    pub fn with_str<'a>(file: Option<Boxed<String>>, src: &'a str) -> Span {
        Span {
            file,
            src: Boxed::new(src.to_string()),
            start: 0,
            end: src.len(),
        }
    }

    pub fn join(self, other: Span) -> Option<Span> {
        if self.start != other.end || self.file != other.file || !Boxed::ptr_eq(&self.src, &other.src) {
            return None;
        }
        Span::new(self.file, self.src, self.start, other.end)
    }

    pub fn extend(&mut self, end: usize) {
        self.end = end;
    }

    pub fn respan(&self, range: Range<usize>) -> Option<Span> {
        if range.end > self.src.len() {
            return None;
        }
        Some(Span {
            file: self.file.clone(),
            src: Boxed::clone(&self.src),
            start: range.start,
            end: range.end,
        })
    }

    pub fn start(&self) -> usize {
        self.start
    }

    pub fn end(&self) -> usize {
        self.end
    }

    pub fn position(&self) -> Position {
        let mut position = Position {
            line: 1,
            col: 1,
            indent: 0,
        };
        for (i, byte) in self.src.bytes().enumerate() {
            if i == self.start {
                break;
            }
            if byte == b' ' || byte == b'\t' {
                position.indent += 1;
                position.col += 1;
            } else if byte == b'\n' {
                position.line += 1;
                position.col = 1;
                position.indent = 0;
            } else {
                position.col += 1;
            }
        }
        position
    }

    pub fn file(&self) -> Option<&Boxed<String>> {
        self.file.as_ref()
    }

    pub fn src(&self) -> &Boxed<String> {
        &self.src
    }

    pub fn as_str(&self) -> &str {
        self.as_ref()
    }
}

impl Deref for Span {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl AsRef<str> for Span {
    fn as_ref(&self) -> &str {
        &self.src[self.start..self.end]
    }
}
