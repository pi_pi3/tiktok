#![warn(rust_2018_idioms)]

pub mod span;
pub mod pattern;
pub mod tokenizer;
