use crate::span::Span;

// #[derive(Token)]
// #[kind(TokenKind)]
pub trait Token: Sized {
    type TokenKind: TokenKind;
    fn try_match(slice: &Span) -> Option<(Span, Self)>;
}

// #[derive(TokenKind)]
//   #[pattern(Pattern)]
pub trait TokenKind: Sized {
    fn try_match(slice: &Span) -> Option<(Span, Span, Self)>;
}

// #[derive(TokenIter)]
pub trait Tokenizer: Iterator {
    type Token: Token;
    type Error;

    fn next(&mut self) -> Option<Result<Self::Token, Self::Error>>;
}
